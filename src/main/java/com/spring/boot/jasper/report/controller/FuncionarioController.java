package com.spring.boot.jasper.report.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.spring.boot.jasper.report.entidades.Funcionario;
import com.spring.boot.jasper.report.service.FuncionarioService;

@RestController
@RequestMapping("/funcionario")
public class FuncionarioController {

	@Autowired
	private FuncionarioService usuarioService;
	
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping("/cadastro")
	public void grava(@RequestBody Funcionario funcionario) {
		usuarioService.salva(funcionario);
		ResponseEntity.ok();
	}
	
	@GetMapping("/lista")
	public ResponseEntity<?> lista() {
		return ResponseEntity.ok(usuarioService.lista());
	}
	
	@GetMapping( value = "relatorio/pdf", produces = MediaType.APPLICATION_PDF_VALUE)
	public ResponseEntity<?> consultarRelatorioPDF() {
		try {
			byte[] bytesPdf = usuarioService.relatorioPdf();
			if(bytesPdf == null) {
				return ResponseEntity.notFound().build();
			}
			
			HttpHeaders headers = new HttpHeaders();
			headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=relatorio-funcionarios.pdf");
			
			return ResponseEntity.ok().contentType(MediaType.APPLICATION_PDF).headers(headers).body(bytesPdf);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}
	
}
