# Spring-boot-jasper-report

Exemplo de API requisitando relatório em PDF com jasperReport.

## Tecnologias utilizadas

* Java, versão: 8
* Maven
  * Spring Boot, versão: 2.1.13.RELEASE
  * Spring Data Jpa
  * Spring boot devtools
  * H2 Database
  * Lombok
  * Jasperreports, versão: 6.12.0

## Urls

Todas as urls responderão no formato **JSON**.

## Funcionário
#### Cadastra funcionário

```
POST - http://localhost:8091/funcionario/cadastro
```

Request
```
{
    "nmFuncionario": "string",
    "nmEmpresa": "string",
    "dsFuncao": "string",
    "vlSalario": "string"
}
```

Response 
```
O retorno será o código HTTP STATUS CODE 201, CREATED.
```

#### Recupera lista de funcionários

```
GET - http://localhost:8091/funcionario/lista
```

Request
```
Nenhum parâmetro passado na requisição.
```

Response 
```
[
    {
        "id": number,
        "nmFuncionario": "string",
        "nmEmpresa": "string",
        "dsFuncao": "string",
        "vlSalario": "string"
    }
]
```

#### Recupera relatório em PDF com a lista de funcionários

```
GET - http://localhost:8091/funcionario/relatorio/pdf
```

Request
```
Nenhum parâmetro passado na requisição.
```

Response 
```
Relatório em PDF.
```

## BANCO DE DADOS H2 - BROWSER URL CONSOLE: 
```
http://localhost:8091/h2-console
```
