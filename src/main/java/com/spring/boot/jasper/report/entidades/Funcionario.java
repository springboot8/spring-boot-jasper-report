package com.spring.boot.jasper.report.entidades;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
public class Funcionario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter @Setter
	private Long id;

	@Getter @Setter
	private String nmFuncionario;

	@Getter @Setter
	private String nmEmpresa;

	@Getter @Setter
	private String dsFuncao;
	
	@Getter @Setter
	private BigDecimal vlSalario;
	
}
