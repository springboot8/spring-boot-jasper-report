package com.spring.boot.jasper.report.service;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import com.spring.boot.jasper.report.entidades.Funcionario;
import com.spring.boot.jasper.report.repository.FuncionarioRepository;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class FuncionarioService {

	@Autowired
	private FuncionarioRepository funcionarioRepository;
	
	public void salva(Funcionario funcionario) {
		funcionarioRepository.save(funcionario);
	}
	
	public List<Funcionario> lista() {
		List<Funcionario> listaFuncionarios = new ArrayList<>();
		funcionarioRepository.findAll().forEach( funcionario -> listaFuncionarios.add(funcionario));
		return listaFuncionarios;
	}
	
	public byte[] relatorioPdf() throws Exception {
		try {
			File file = ResourceUtils.getFile("classpath:relatorios/relatorio-funcionarios.jasper");
			try(FileInputStream inputStream = new FileInputStream(file)) {
				JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(lista());
				Map<String, Object> parameters = new HashMap<>();
				parameters.put("REPORT_LOCALE", new Locale("pt", "BR"));
				JasperPrint jasperPrint = JasperFillManager.fillReport(inputStream, parameters, dataSource); 
				return JasperExportManager.exportReportToPdf(jasperPrint);
			}
		} catch(Exception e) {
			throw new Exception("Não foi possível emitir o relatório.");
		}
	}
	
}
